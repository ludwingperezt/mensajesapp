package com.ludwing.mensajesapp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MensajesDAO {

	public static void crearMensajesDB(Mensajes mensaje) {
		
		Conexion con = new Conexion();
		
		try (Connection dbCon = con.getConnection()) {
			PreparedStatement ps = null;
			
			try {
				String query = "INSERT INTO `mensajes`(`mensaje`, `autor_mesaje`) VALUES (?, ?)";
				ps = dbCon.prepareStatement(query);
				ps.setString(1, mensaje.getMensaje());
				ps.setString(2, mensaje.getAutorMensaje());
				
				ps.executeUpdate();
				System.out.println("Mensaje creado");
			}
			catch (SQLException ex) {
				System.out.println(ex);
			}
			
		}
		catch (SQLException e) {
			System.out.println(e);
		}
		
	}
	
	public static void leerMensajesDB() {
		
		Conexion con = new Conexion();
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try (Connection dbCon = con.getConnection()) {
			String query = "SELECT `id_mensaje`, `mensaje`, `autor_mesaje`, `fecha_mensaje` FROM `mensajes`";
			ps = dbCon.prepareStatement(query);
			
			rs = ps.executeQuery();
			
			while (rs.next()) {
				System.out.println("ID: "+ rs.getInt("id_mensaje"));
				System.out.println("Mensaje: "+ rs.getString("mensaje"));
				System.out.println("Autor: "+ rs.getString("autor_mesaje"));
				System.out.println("Fecha: "+ rs.getString("fecha_mensaje"));
				System.out.println("");
			}
			
		}
		catch (SQLException e) {
			System.out.println("No se pudo leer mensajes");
			System.out.println(e);
		}
		
	}
	
	public static void eliminarMensajeDB(int idMensaje) {
		Conexion con = new Conexion();
		
		try (Connection dbCon = con.getConnection()) {
			String query = "DELETE FROM `mensajes` WHERE id_mensaje = ?";
			
			try {
				PreparedStatement ps = dbCon.prepareStatement(query);
				ps.setInt(1, idMensaje);
				ps.executeUpdate();
				System.out.println("Se borró el mensaje");
			}
			catch (SQLException e) {
				System.out.println(e);
			}
		}
		catch (SQLException e) {
			System.out.println("No se pudo eliminar mensaje");
			System.out.println(e);
		}
	}
	
	public static void actualizarMensajeDB(Mensajes mensaje) {
		Conexion con = new Conexion();
		
		try (Connection dbCon = con.getConnection()) {
			PreparedStatement ps = null;
			
			try {
				String query = "UPDATE mensajes "
						+ "SET "
						+ "mensaje = ?"
						+ " WHERE id_mensaje = ?";
				ps = dbCon.prepareStatement(query);
				ps.setString(1, mensaje.getMensaje());
				ps.setInt(2, mensaje.getIdMensaje());
				
				ps.executeUpdate();
				System.out.println("Mensaje actualizado");
			}
			catch (SQLException ex) {
				System.out.println(ex);
			}
			
		}
		catch (SQLException e) {
			System.out.println(e);
		}
	}
}
