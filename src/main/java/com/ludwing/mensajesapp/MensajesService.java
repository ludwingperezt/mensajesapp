package com.ludwing.mensajesapp;

import java.util.Scanner;

public class MensajesService {
	
	public static void crearMensaje() {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Ingrese el mensaje:");
		String textoMensaje = sc.nextLine();
		
		System.out.println("Autor:");
		String textoAutor = sc.nextLine();
		
		Mensajes mensaje = new Mensajes();
		mensaje.setMensaje(textoMensaje);
		mensaje.setAutorMensaje(textoAutor);
		
		MensajesDAO.crearMensajesDB(mensaje);
		//sc.close();
	}
	
	public static void listarMensajes() {
		
		MensajesDAO.leerMensajesDB();
		
	}
	
	public static void borrarMensaje() {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Ingrese el ID del mensaje a borrar:");
		int idMensaje = sc.nextInt();
				
		MensajesDAO.eliminarMensajeDB(idMensaje);
		//sc.close();
	}
	
	public static void editarMensaje() {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Ingrese el mensaje:");
		String textoMensaje = sc.nextLine();
		
		System.out.println("ID del mensaje:");
		int idMensaje = sc.nextInt();
		
		Mensajes mensaje = new Mensajes();
		mensaje.setMensaje(textoMensaje);
		mensaje.setIdMensaje(idMensaje);
		
		MensajesDAO.actualizarMensajeDB(mensaje);
	}

}
